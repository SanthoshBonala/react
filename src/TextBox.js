import React from 'react'
const My =(props) =>{
  let style={
   display : "inline-block",
   padding: "16px",
   textAlign : "Center",
   margin: "16px",
   border: "1px solid black",
   backgroundColor: "black",
   color: "white" 
   
  }
  return(
      <div style={style} onClick={() => props.clicked()}>
      <p >{props.char}</p>
      </div>
  );

}
export default My;