import React, { Component } from 'react';
import './App.css';
import Valid from './Comp';
import My from './TextBox';

class App extends Component {
   
    state = {
       text: "hi",
       ans: 2
     }
  
   NameChangeHandler = (evt) => {
    let val = evt.target.value;
    let len = val.length;
    console.log("------------"+len)
    this.setState({
      text: val,
      ans: len
    }) 
  }
deleteHandler= (index) =>{
let charList= this.state.text.split("")
charList.splice(index,1)
let charString = charList.join("")
this.setState({
  text: charString,
  ans: charString.length
});

}
render() {
    
  let chars = this.state.text.split('').map((char, index) => {
    return <My char={char} key={index} clicked={() => this.deleteHandler(index)} />;
  });
  
    return (
    <div className="App">
        <p><br />{this.state.text}{this.state.ans}</p>
        <input type="text" name="text" value={this.state.text}  onChange={this.NameChangeHandler}/>
        <Valid lengt={this.state.ans} />  
        {chars} 
        <footer>Click on Black box to delete a character</footer> 
      </div>
    );
  }
}

export default App;
